import cv2 as cv


# Given an image returns the background
def get_background(img):
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    retval, dst = cv.threshold(gray, 140, 255, cv.THRESH_BINARY)
    return dst


# Given an image and its background, subtracts the background from the image
def get_image_without_background(img, backg):
    img = cv.cvtColor(img, cv.COLOR_RGB2RGBA)
    img[:, :, 3] = backg

    # HERE: Uncomment if you want to get the image without background
    # cv.imwrite("resultado.png", img)

    return img


# Shows the camera with the image given on the top-left corner
def camera(img, backg):
    img_resized = resize(img)
    backg_resized = resize(backg)
    backg_resized_inv = cv.bitwise_not(backg_resized)

    rows, cols, channels = img_resized.shape

    cap = cv.VideoCapture(0)
    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            print("Can't receive frame (stream end?). Exiting ...")
            break

        roi = frame[0:rows, 0:cols]

        roi_bg = cv.bitwise_and(roi, roi, mask=backg_resized_inv)
        roi_fg = cv.bitwise_and(img_resized, img_resized, mask=backg_resized)

        roi_final = cv.add(roi_bg, roi_fg)
        frame[0:rows, 0:cols] = roi_final

        cv.imshow("Video_final", frame)

        if cv.waitKey(1) == ord('q'):
            break

    # Release everything
    cap.release()
    cv.destroyAllWindows()


# Given an image, resize it in a specific percentage
def resize(img):
    scale_percent = 15  # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)

    # resize image
    return cv.resize(img, dim, interpolation = cv.INTER_AREA)


# Launch
if __name__ == '__main__':
    image = cv.imread("monedicas.jpeg")
    background = get_background(image)
    get_image_without_background(image, background)
    camera(image, background)


