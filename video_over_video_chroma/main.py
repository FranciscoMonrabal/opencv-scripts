import cv2 as cv
import numpy as np


# Sorts the polygon's coordinates array
def get_coor_order(coor):

    x = [coor[0][0][0], coor[1][0][0], coor[2][0][0], coor[3][0][0]]
    y = [coor[0][0][1], coor[1][0][1], coor[2][0][1], coor[3][0][1]]
    ret = []

    ret.insert(0, min(range(len(x)), key=x.__getitem__))
    ret.insert(1, min(range(len(y)), key=y.__getitem__))
    ret.insert(2, max(range(len(y)), key=y.__getitem__))
    ret.insert(3, max(range(len(x)), key=x.__getitem__))

    return ret


# Apply the threshold to the given image and returns it
def apply_threshold(img):
    ret, thresh = cv.threshold(img, 65, 255, cv.THRESH_BINARY_INV)
    return thresh


# Gets the 4 points of the polygon, the image given must be grayscale
def get_rectangle(img):
    values, contours, hierarchy = cv.findContours(img, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key=cv.contourArea, reverse=True)               # Sort from bigger to smaller
    aprox = cv.approxPolyDP(contours[0], 10, True)                               # Reduce the number of coordinates
    order = get_coor_order(aprox)
    aprox = np.float32([aprox[order[0]][0], aprox[order[1]][0], aprox[order[2]][0], aprox[order[3]][0]])    # Prepare the shape of the array

    return aprox


# Transforms the insert_image into the shape necessary, and draws it in the background image
def transform_and_join(img, ins_img, thresh, thresh_inv, rect_coor):
    # Coordinates and dimensions
    rows, cols, channels = ins_img.shape
    height, width, ignore = img.shape
    ins_img_dimensions = np.float32([[0, 0], [cols, 0], [0, rows], [cols, rows]])

    # Shape transformation
    mask = cv.getPerspectiveTransform(ins_img_dimensions, rect_coor)
    out = cv.warpPerspective(ins_img, mask, (width, height))

    # Draw
    bg = cv.bitwise_and(img, img, mask=thresh_inv)
    fg = cv.bitwise_and(out, out, mask=thresh)

    return cv.add(bg, fg)


# Opens the videos and works with them
def video():
    cap = cv.VideoCapture('background_video.mp4')
    ins_video = cv.VideoCapture('chroma_video.mp4')

    while cap.isOpened():
        ret, image = cap.read()
        ret1, ins_image = ins_video.read()
        if not ret or not ret1:
            print("Can't receive frame (stream end?). Exiting ...")
            break

        threshold = apply_threshold(image[:, :, 2])
        threshold_inv = cv.bitwise_not(threshold)
        rectangle = get_rectangle(threshold)

        final = transform_and_join(image, ins_image, threshold, threshold_inv, rectangle)

        cv.imshow("final", final)
        cv.imshow("th", threshold)
        cv.imshow("oriG", image[:, :, 2])

        if cv.waitKey(1) == ord('q'):
            break

    # Release everything if job is finished
    cap.release()
    cv.destroyAllWindows()


if __name__ == '__main__':
    video()
