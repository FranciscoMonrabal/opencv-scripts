import cv2 as cv
import numpy as np


# Apply the threshold to the given image and returns it
def apply_threshold(img):
    ret, thresh = cv.threshold(img, 80, 255, cv.THRESH_BINARY)
    return thresh


def apply_median_blur(img):
    median = cv.medianBlur(img, 5)
    return median


def apply_erode(img):
    kernel = np.ones((5, 5), np.uint8)
    erode = cv.erode(img, kernel, iterations=5)
    return erode


# Calculates the ball frame by frame
def camera():

    cap = cv.VideoCapture('video.mp4')
    f = open('coordinates.csv', 'a')
    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            print("Can't receive frame (stream end?). Exiting ...")
            break

        image_thresh = apply_threshold(frame[:, :, 0])
        image_median = apply_median_blur(image_thresh)
        image_erode = apply_erode(image_median)

        rows, cols, channels = frame.shape

        # Create a synthetic frame for alter drawing
        syn_frame = np.zeros((rows, cols, 3), np.uint8)

        circles = cv.HoughCircles(image_erode,
                                  cv.HOUGH_GRADIENT,
                                  1,
                                  rows / 8,
                                  param1=40,
                                  param2=15,
                                  minRadius=90,
                                  maxRadius=200
                                  )

        if circles is not None:
            circles = np.uint16(np.around(circles))
            for i in circles[0, :]:
                center = (i[0], i[1])

                # DRAW IN THE FRAME
                # circle center
                cv.circle(frame, center, 1, (0, 100, 100), 3)
                # circle outline
                radius = i[2]
                cv.circle(frame, center, radius, (255, 0, 255), 3)

                # DRAW IN A SYNTHETIC FRAME
                cv.circle(syn_frame, center, 100, (0, 0, 255), -1)

                # Write in the CSV file
                f.write(f'{cap.get(cv.CAP_PROP_POS_MSEC)},{center[0]},{center[1]}\n')

        cv.imshow("video_final", resize(frame))
        cv.imshow("video_final_erode", resize(image_erode))
        cv.imshow("syn_video", resize(syn_frame))

        if cv.waitKey(1) == ord('q'):
            break

    # Release everything
    cap.release()
    cv.destroyAllWindows()
    f.close()


# Given an image, resize it in a specific percentage
def resize(img):
    scale_percent = 50  # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)

    # resize image
    return cv.resize(img, dim, interpolation = cv.INTER_AREA)


if __name__ == '__main__':
    camera()



