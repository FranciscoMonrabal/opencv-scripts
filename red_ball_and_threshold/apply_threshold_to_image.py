import cv2 as cv


# Apply the threshold to the given image
def apply_threshold(img):
    ret, thresh = cv.threshold(img, 140, 255, cv.THRESH_BINARY)
    cv.imshow("thresh", thresh)

    cv.waitKey(0)
    cv.destroyAllWindows()


# Given an image, resize it in a specific percentage
def resize(img):
    scale_percent = 50  # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)

    # resize image
    return cv.resize(img, dim, interpolation = cv.INTER_AREA)


if __name__ == '__main__':
    apply_threshold(resize(cv.imread("apuntes.jpeg", cv.IMREAD_GRAYSCALE)))


